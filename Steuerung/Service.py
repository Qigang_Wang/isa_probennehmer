# coding: utf-8
# tcp stream server
import socket
import logging
import time
from threading import Thread, current_thread
import json
import os
from Steuerung import ZentralSteuerung
from SteuerungZusatz import ZusatzSteuerung
from SteuerungStromMessung import StromMessung
from Backup import Backup
import math
from Datenbank import dbConnection
from Settings import Settings
import sys
from Settings import Settings
from Watchdog import Watchdog

class Server:
    Zustand = {'Messung': False,
               'Zusatz': False,
               'Backup': False,
               'Strommessung': False,
               'Messdaten': {}}
    Steuerung = None
    Backup = None
    TCPLength = 1024
    Log = logging
    _sock = None

    def __init__(self):
        self.Initialisierung()

        while True:
            self.Log.info("Warten  auf Verbindung...")
            conn, addr = self._sock.accept()
            self.Log.info("Eine neue Verbindung: %s from %s", conn, addr)
            Thread(target=self.TCP_Hauptprozess, args=(conn,)).start()

    def Initialisierung(self):
        self.LogInitialisierung()
        self.Log.info('- ' * 15)
        self.Log.info('Log initialisiert.')
        # Initialisierung der Backup
        self.Backup = Backup()
        self.Backup.Log = self.Log
        self.Backup.Logandprint('Backup Initialisiert')
        self.Backup.Starten()
        # Initialisierung der Ventilsteuerung
        self.Steuerung = ZentralSteuerung()
        self.Steuerung.Log = self.Log
        self.Steuerung.Logandprint('Ventilsteuerung initialisiert')
        # Initialisierung der Zusatzsteuerung
        self.Zusatzsteuerung = ZusatzSteuerung()
        self.Zusatzsteuerung.Log = self.Log
        self.Steuerung.Logandprint('Zusatzventilsteuerung initialisiert')
        # Initialisierung der Strommessung
        self.StromMessungSteuerung = StromMessung()
        self.StromMessungSteuerung.Log = self.Log
        # self.StromMessungSteuerung.Starten()

        self.Setting = Settings().Setting

        self.SocketInitialisierung()
        self.Log.info('Initialisierung ist fertig.')

    def LogInitialisierung(self):
        self.LogName = f'Logging_{time.localtime().tm_year}_{time.localtime().tm_mon}.log'
        if sys.platform == 'win32':
            logging.basicConfig(
                level=logging.INFO,
                format="%(asctime)s %(levelname)s %(process)s %(threadName)s |%(message)s",
                filename=self.LogName,
            )
        else:
            logging.basicConfig(
                level=logging.INFO,
                format="%(asctime)s %(levelname)s %(process)s %(threadName)s |%(message)s",
                filename=self.LogName,
            )
        self.Log = logging


    def SocketInitialisierung(self):
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.bind(('', 8000))
        """ Start Service """
        self._sock.listen()
        self.Log.info('Socket initialisiert')
        self.Log.info("Start Serving...")


    def TCP_Hauptprozess(self, conn: socket.socket = None):
        """ receive a socket connection"""
        # Db = dbConnection()
        # DbStromMesssung = dbConnection(Datenbank='Strommessdaten.db')
        self.LogInitialisierung()
        while True:
            if not conn:
                self.Log.info('Es gibt keine Verbindung mehr!')
                return
            try:
                ReceiveData = self.TCP_Nachricht_empfangen(conn)
                if len(ReceiveData) == 0:
                    continue
                TCP_Empfang = json.loads(ReceiveData)
            except Exception as e:
                self.Log.info("Empfang kaputt: %s", e)
                return
            if 'getZustand' not in ReceiveData:
                self.Log.info("[R %s]<< %s", current_thread().name, TCP_Empfang)

            if TCP_Empfang['Befehl'] == 'Close':
                self.Log.info("Verbindung schließt")
                conn.close()
                return

            elif TCP_Empfang['Befehl'] == 'MessungStarten':
                self.Steuerung.Starten()
                time.sleep(1)
                Watchdog(self.Steuerung)
                self.Zustand['Messung'] = True

            elif TCP_Empfang['Befehl'] == 'MessungBeenden':
                self.Steuerung.Beenden()
                self.Zustand['Messung'] = False

            elif TCP_Empfang['Befehl'] == 'BackupStarten':
                self.Backup.Starten()
                self.Zustand['Backup'] = True

            elif TCP_Empfang['Befehl'] == 'BackupBeenden':
                self.Backup.Beenden()
                self.Zustand['Backup'] = False

            elif TCP_Empfang['Befehl'] == 'ZusatzVentilStarten':
                self.Zusatzsteuerung.Starten()
                self.Zustand['Zusatz'] = True

            elif TCP_Empfang['Befehl'] == 'ZusatzVentilStop':
                self.Zusatzsteuerung.Beenden()
                self.Zustand['Zusatz'] = False

            elif TCP_Empfang['Befehl'] == 'StromMessenStarten':
                self.StromMessungSteuerung.Starten()
                self.Zustand['Strommessung'] = True

            elif TCP_Empfang['Befehl'] == 'StromMessenStop':
                self.StromMessungSteuerung.Beenden()
                self.Zustand['Strommessung'] = False

            elif TCP_Empfang['Befehl'] == 'StromMessdatenExport':
                DbStromMesssung = dbConnection(Datenbank='Strommessdaten.db')
                DbStromMesssung.save_Strommessadaten_as_excel()
                self.Datei_senden(conn, DbStromMesssung.ExcelName)
                DbStromMesssung.conn.close()

            elif TCP_Empfang['Befehl'] == 'getMessdaten':
                self.sendenDict(conn, self.Steuerung.Aktuelle_Messdaten)

            elif TCP_Empfang['Befehl'] == 'BackupSofort':
                self.Backup.Backup_Aller_Datenbank()

            elif TCP_Empfang['Befehl'] == 'MessdatenExportieren':
                Db = dbConnection()
                Db.save_as_excel()
                self.Datei_senden(conn, Db.ExcelName)
                Db.conn.close()
            elif TCP_Empfang['Befehl'] == 'getLog':
                self.Datei_senden(conn, self.LogName)
            elif TCP_Empfang['Befehl'] == 'EinstellungChange':
                self.Settings = Settings()
                New_Setting = TCP_Empfang['Wert']
                for key in New_Setting.keys():
                    self.Settings.Setting[key] = New_Setting[key]
                self.Settings.save_settings()
                self.Log.info(self.Settings.Setting)

                self.Steuerung.Beenden()
                if self.Zustand['Messung']:
                    self.Steuerung.Starten()

                self.Backup.Beenden()
                if self.Zustand['Backup']:
                    self.Backup.Starten()

                self.Zusatzsteuerung.Beenden()
                if self.Zustand['Zusatz']:
                    self.Zusatzsteuerung.Starten()

                self.StromMessungSteuerung.Beenden()
                if self.Zustand['Strommessung']:
                    self.StromMessungSteuerung.Starten()

                self.Log.info("Einstellung geändert")

            elif TCP_Empfang['Befehl'] == 'ReihenFolgeHochladen':
                time.sleep(0.1)
                self.DateiEmpfangen(conn)
                time.sleep(0.1)
                self.Steuerung.Ventilablauf_Ablesen()
                time.sleep(0.1)
                self.Zusatzsteuerung.Ventilablauf_Ablesen()
            elif TCP_Empfang['Befehl'] == 'getZustand':
                self.Zustand['Messung'] = self.Steuerung.Betriebszustand
                self.Zustand['Backup'] = self.Backup.Betriebszustand
                self.Zustand['Messdaten'] = self.Steuerung.Aktuelle_Messdaten
                self.Zustand['Zusatz'] = self.Zusatzsteuerung.Betriebszustand
                self.sendenDict(conn, self.Zustand)
            elif TCP_Empfang['Befehl'] == 'SettingAbholen':
                self.sendenDict(conn, self.Setting)

    def TCP_Nachricht_senden(self, conn: socket.socket = None, Nachricht: str = ''):
        NachrichtLength = len(Nachricht)
        try:
            if NachrichtLength > self.TCPLength:
                Anzahl = math.ceil(NachrichtLength / self.TCPLength)
                # LengthInformation = {'MultiNachricht': True, 'Anzahl': Anzahl}
                #
                # conn.send(json.dumps(LengthInformation).encode("utf-8"))
                NachrichtCodierung = Nachricht.encode("utf-8")
                conn.send('**Start**'.encode('utf-8'))
                for i in range(Anzahl):
                    conn.send(NachrichtCodierung[0 + i * self.TCPLength:self.TCPLength + i * self.TCPLength])
                conn.send('**Stop**'.encode('utf-8'))
            else:
                conn.send(Nachricht.ljust(self.TCPLength).encode("utf-8"))
        except Exception as e:
            self.Log.info("TCP Nachricht senden failed: %s-%s", Nachricht, e)

    def sendenDict(self, conn: socket.socket = None, Nachricht: dict = {}):
        self.TCP_Nachricht_senden(conn, json.dumps(Nachricht))

    def TCP_Nachricht_empfangen(self, conn: socket.socket = None):
        Nachrichten = conn.recv(self.TCPLength).decode('utf-8')
        if '**Start**' in Nachrichten:
            while '**Stop**' not in Nachrichten:
                Nachrichten = Nachrichten + conn.recv(self.TCPLength).decode('utf-8')
            Start = Nachrichten.find('**Start**')
            Stop = Nachrichten.find('**Stop**')
            Ergebnis = Nachrichten[Start + 9:Stop]
        else:
            Ergebnis = Nachrichten
        return Ergebnis

    def Datei_senden(self, conn: socket.socket = None, Filename: str = "Reihenfolge.xlsx"):
        Header_Dic = {'filename': Filename,
                      'filesize': os.path.getsize(Filename)}
        self.sendenDict(conn, Header_Dic)
        time.sleep(0.5)
        with open('%s/%s' % (os.getcwd(), Filename), 'rb') as f:
            for line in f:
                conn.send(line)

    def DateiEmpfangen(self, conn: socket.socket = None):
        Header = json.loads(self.TCP_Nachricht_empfangen(conn))
        with open(os.path.join(os.getcwd(), Header['filename']), 'wb') as f:
            recv_size = 0
            while recv_size < Header['filesize']:
                line = conn.recv(self.TCPLength*2)
                f.write(line)
                recv_size += len(line)


if __name__ == '__main__':
    s = Server()
