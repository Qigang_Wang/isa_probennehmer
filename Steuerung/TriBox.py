from pymodbus.client import ModbusTcpClient
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.constants import Endian
from Settings import Settings
import time


class TriBox_ModBus:
    ModBusVerbindung = None

    def __init__(self):
        self.Initialisierung()
        self.ModBusVerbindung.connect()
        self.Zeit_Aktualisieren()
        self.ModBusVerbindung.close()

    def Initialisierung(self):
        Setting = Settings().Setting
        self.ModBusVerbindung = ModbusTcpClient(host=Setting['Tribox']['ip'], port=Setting['Tribox']['port'])

    def Beenden(self):
        self.ModBusVerbindung.close()

    def Ablesen(self, Adresse, Anzahl, unit=3):
        self.ModBusVerbindung.connect()
        time.sleep(0.5)
        Ergebnis = self.ModBusVerbindung.read_holding_registers(address=Adresse, count=Anzahl, slave=unit).registers
        self.ModBusVerbindung.close()
        return Ergebnis

    def Schreiben(self, Adresse, values, unit):
        self.ModBusVerbindung.connect()
        self.ModBusVerbindung.write_registers(address=Adresse, values=values, slave=unit)
        Ergebnis = self.ModBusVerbindung.read_holding_registers(address=Adresse, count=values.__len__(),
                                                                slave=unit).registers
        self.ModBusVerbindung.close()
        Flag = False
        if values == Ergebnis:
            Flag = True
        return Flag

    def To32Flot(self, Data, Position):
        return BinaryPayloadDecoder.fromRegisters(Data[Position: Position + 2], byteorder=Endian.Big,
                                                  wordorder=Endian.Big).decode_32bit_float()

    def Messdaten_Ablesen(self):
        Ergebnis = self.Ablesen(Adresse=1000, Anzahl=100)

        Messdaten = {}

        Messung = {"Wert": self.To32Flot(Ergebnis, 0),
                   "Einheit": 'mg/l'}
        Messdaten["NO3_N"] = Messung

        Messung = {"Wert": self.To32Flot(Ergebnis, 2),
                   "Einheit": 'mg/l'}
        Messdaten["NO2_N"] = Messung

        Messung = {"Wert": self.To32Flot(Ergebnis, 4),
                   "Einheit": 'mg/l'}
        Messdaten["CSBeq"] = Messung

        Messung = {"Wert": self.To32Flot(Ergebnis, 16),
                   "Einheit": 'mg/l'}
        Messdaten["TSSeq"] = Messung

        Messung = {"Wert": self.To32Flot(Ergebnis, 32),
                   "Einheit": '1/m'}
        Messdaten["SAK254"] = Messung

        Messung = {"Wert": self.To32Flot(Ergebnis, 34),
                   "Einheit": 'AU'}
        Messdaten["ABS360"] = Messung

        Messung = {"Wert": self.To32Flot(Ergebnis, 36),
                   "Einheit": 'AU'}
        Messdaten["ABS210"] = Messung

        Messung = {"Wert": self.To32Flot(Ergebnis, 38),
                   "Einheit": ''}
        Messdaten["Fit_Error"] = Messung

        Messung = {"Wert": self.To32Flot(Ergebnis, 42),
                   "Einheit": 'AU'}
        Messdaten["ABS254"] = Messung

        Messung = {"Wert": self.To32Flot(Ergebnis, 46),
                   "Einheit": 'mg/l'}
        Messdaten["NO3"] = Messung

        Messung = {"Wert": self.To32Flot(Ergebnis, 48),
                   "Einheit": 'mg/l'}
        Messdaten["NO2"] = Messung

        Messung = {"Wert": self.To32Flot(Ergebnis, 52),
                   "Einheit": 'mg/l'}
        Messdaten["COD_SACeq"] = Messung

        Messung = {"Wert": self.To32Flot(Ergebnis, 56),
                   "Einheit": 'mg/l'}
        Messdaten["TOC_SACeq"] = Messung

        Messung = {"Wert": self.To32Flot(Ergebnis, 58),
                   "Einheit": 'mg/l'}
        Messdaten["DOC_SACeq"] = Messung

        Messung = {"Wert": self.To32Flot(Ergebnis, 60),
                   "Einheit": ''}
        Messdaten["SQI"] = Messung

        Messung = {"Wert": self.To32Flot(Ergebnis, 62),
                   "Einheit": '%'}
        Messdaten["UVT254"] = Messung

        return Messdaten

    def Kurve_ablesen(self):
        Wavelength = []
        Absorption = []
        Messdaten = {}
        i = 0
        # for i in range(40):
        while True:
            if i % 25 == 0:
                Ergebnis = self.Ablesen(Adresse=2100 + i * 4, Anzahl=100)
            Position = (i % 25) * 4
            i = i + 1
            Wavelength_i = self.To32Flot(Ergebnis, Position)
            Absorption_i = self.To32Flot(Ergebnis, Position + 2)

            if Wavelength_i == 0:
                break
            Wavelength.append(Wavelength_i)
            Absorption.append(Absorption_i)
        Messdaten["Wavelength"] = Wavelength
        Messdaten["Absorption"] = Absorption
        return Messdaten

    def generateMessdaten(self):
        self.Zeit_Aktualisieren()
        Messdaten = self.Messdaten_Ablesen()
        Messdaten["Kurve"] = {"Wert": self.Kurve_ablesen()}
        return Messdaten

    def Zeit_Aktualisieren(self):
        LokalZeit = time.localtime()
        Zeit = [LokalZeit.tm_year, LokalZeit.tm_mon, LokalZeit.tm_mday, LokalZeit.tm_hour, LokalZeit.tm_min,
                LokalZeit.tm_sec]
        return self.Schreiben(Adresse=102, values=Zeit, unit=1)

    def Zeit_Letzte_Messung(self):
        Zeit = self.Ablesen(Adresse=81, Anzahl=6, unit=3)
        Zeitstr = f"{Zeit[0]}-{Zeit[1]}-{Zeit[2]} {Zeit[3]}:{Zeit[4]}:{Zeit[5]}"
        if Zeitstr == "0-0-0 0:0:0":
            timestamp = time.mktime(time.strptime("2020-02-02 02:02:02", "%Y-%m-%d %H:%M:%S"))
        else:
            timestamp = time.mktime(time.strptime(Zeitstr, "%Y-%m-%d %H:%M:%S"))
        return timestamp


if __name__ == "__main__":
    TriBox_ = TriBox_ModBus()

    # print(TriBox_.Messdaten_Ablesen())
    # print(TriBox_.Ablesen(Adresse=81, Anzahl=6, unit=3))
    print(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(TriBox_.Zeit_Letzte_Messung())))
    #
    # Kurve =Messdaten["Kurve"]

    # # print(Kurve)
    # plt.plot(Kurve["Wavelength"], Kurve["Absorption"], label="sigmoid")
    # plt.xlabel("x")
    # plt.ylabel("y")
    # plt.legend()
    # plt.show()
