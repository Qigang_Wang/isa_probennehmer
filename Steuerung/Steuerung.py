import pandas as pd
import threading
import time
from Unipi import UniPiClient
from TriBox import TriBox_ModBus
from Datenbank import dbConnection
from Settings import Settings
from Mailservice import sendEmail


class ZentralSteuerung(object):
    Ventilablauf = {}
    Thread_Ventilsteuerung = None
    Betriebszustand = False
    MessungFertig = False
    Aktuelle_Messdaten = {}
    TriBox = None
    Unipi = None
    Setting = None
    Datenbankverbindung = None
    Log = None
    Messungszustand = {"Fehler": False,
                       'Fehlerzähler':0}

    def __init__(self):
        self.WatchdogTime = time.time()

    def Logandprint(self, log: str):
        self.Log.info(log)

    def Starten(self):
        if not self.Betriebszustand:
            self.Setting = Settings().Setting
            self.Ventilablauf_Ablesen()
            self.Unipi = UniPiClient()
            self.TriBox = TriBox_ModBus()
            self.Betriebszustand = True
            self.Thread_Ventilsteuerung = threading.Thread(target=self.Ventilsteuerung_Schedule, daemon=True)
            self.Thread_Ventilsteuerung.start()
            self.Logandprint('Die Messung wird gestartet!')
        else:
            self.Logandprint('Die Messung läuft noch!')

    def Beenden(self):
        if self.Betriebszustand:
            self.Betriebszustand = False
            self.Unipi.Beenden()
            self.TriBox.Beenden()
            self.Logandprint('Messung beendet gerade')
        else:
            self.Logandprint('Messung läuft schon nicht mehr')

    def Ventilablauf_Ablesen(self):
        Ventile_Reihenfolge = pd.read_excel('Reihenfolge.xlsx', sheet_name='Ventile')
        Ventile_Reihenfolge['Zeit'] = Ventile_Reihenfolge['Zeit'].apply(lambda x: x.strftime('%H-%M'))
        Ventile_Reihenfolge = Ventile_Reihenfolge.fillna('')
        Ventile_Reihenfolge['Messen'] = Ventile_Reihenfolge['Messen'].apply(lambda x: x.lower() == "ja")
        Reihenfolge = Ventile_Reihenfolge.set_index('Zeit').transpose().to_dict(orient='dict')
        self.Ventilablauf = Reihenfolge

    def Ventilsteuerung_Schedule(self):
        # self.Datenbankverbindung = dbConnection()
        while self.Betriebszustand:
            self.Ventilsteuerung()
            self.WatchdogTime = time.time()
            time.sleep(5)

    def Ventilsteuerung(self):

        Uhrzeit = time.strftime("%H-%M")
        Startzeit = time.strftime("%Y-%m-%d %H:%M:00")
        # print(time.strftime("%Y-%m-%d %H:%M:%S"))
        if (Uhrzeit in self.Ventilablauf) & ~self.MessungFertig:
            Aktuelle_Ventil = self.Ventilablauf[Uhrzeit]['Ventil']
            self.MessungFertig = True
            if Aktuelle_Ventil != '':
                self.Unipi.StartProbenehmer(Aktuelle_Ventil)
                self.Logandprint(f'Messung mit dem Ventil {Aktuelle_Ventil} wird gestartet')
                if self.Ventilablauf[Uhrzeit]['Messen']:
                    # Überfrüfen, ob eine neue Messung erfolgreich ist.
                    while self.TriBox.Zeit_Letzte_Messung() < \
                            time.mktime(time.strptime(Startzeit, "%Y-%m-%d %H:%M:%S")):
                        if time.time() > time.mktime(time.strptime(Startzeit, "%Y-%m-%d %H:%M:%S")) + 60 * 4:
                            self.Logandprint(f'Zu lange gewartet ')
                            self.Messungszustand['Fehlerzähler']=self.Messungszustand['Fehlerzähler']+1
                            if self.Messungszustand['Fehlerzähler']>= 5 & ~self.Messungszustand['Fehler']:
                                sendEmail(Inhalt=' - Es gibt Fehler in der Ventilsteuerung')
                                self.Messungszustand['Fehler'] = True
                            try:
                                Messergebnis = self.TriBox.generateMessdaten()
                                for key in Messergebnis:
                                    Messergebnis[key]['Wert'] = -1
                                Messergebnis['Ventil'] = {"Wert": Aktuelle_Ventil}
                                Messergebnis['Messzeit'] = {"Wert": Startzeit}
                                self.Aktuelle_Messdaten = Messergebnis
                                Datenbankverbindung = dbConnection()
                                Datenbankverbindung.Save_Messdaten(Messergebnis)
                                Datenbankverbindung.conn.close()
                            except Exception as e:
                                self.Logandprint(f'Zu lange gewartet und es gibt Fehler beim Lesen ')
                            return
                        time.sleep(5)
                        # wenn ja, dan weiter
                    self.Logandprint(f'Messung mit dem Ventil {Aktuelle_Ventil} ist fertig')
                    self.Messungszustand['Fehlerzähler'] = 0
                    self.Messungszustand['Fehler'] = False
                    try:
                        Messergebnis = self.TriBox.generateMessdaten()
                        Messergebnis['Ventil'] = {"Wert": Aktuelle_Ventil}
                        Messergebnis['Messzeit'] = {"Wert": Startzeit}
                        self.Aktuelle_Messdaten = Messergebnis
                        Datenbankverbindung = dbConnection()
                        Datenbankverbindung.Save_Messdaten(Messergebnis)
                        Datenbankverbindung.conn.close()
                    except Exception as e:
                        self.Logandprint(f'Fehler im Ablesen der Messdaten:{e}')
        elif Uhrzeit not in self.Ventilablauf:
            self.MessungFertig = False


if __name__ == '__main__':
    Steuerung = ZentralSteuerung()
    Steuerung.Starten()
    time.sleep(5)
    Steuerung.Beenden()
    time.sleep(3)
