import pandas as pd
import threading
import time
from Unipi import UniPiClient
from Mailservice import sendEmail

class ZusatzSteuerung(object):
    Ventilablauf = {}
    Thread_Ventilsteuerung = None
    Betriebszustand = False
    MessungFertig = False
    Unipi = None
    Log = None

    def __init__(self):
        pass

    def Logandprint(self, log: str):
        if self.Log==None:
            print(log)
        else:
            self.Log.info(log)

    def Starten(self):
        if not self.Betriebszustand:
            self.Ventilablauf_Ablesen()
            print(self.Ventilablauf)
            self.Unipi = UniPiClient()
            self.Betriebszustand = True
            self.Thread_Ventilsteuerung = threading.Thread(target=self.Schedule, daemon=False)
            self.Thread_Ventilsteuerung.start()
            self.Logandprint('Die Messung wird gestartet!')
        else:
            self.Logandprint('Die Messung läuft noch!')

    def Beenden(self):
        if self.Betriebszustand:
            self.Betriebszustand = False
            self.Unipi.Beenden()
            self.Logandprint('Messung beendet gerade')
        else:
            self.Logandprint('Messung läuft schon nicht mehr')

    def Ventilablauf_Ablesen(self):
        Ventile_Reihenfolge = pd.read_excel('Reihenfolge.xlsx', sheet_name='Zusatz')
        Ventile_Reihenfolge['Zeit'] = Ventile_Reihenfolge['Zeit'].apply(lambda x: x.strftime('%H-%M'))
        # Ventile_Reihenfolge['Zeit'] = Ventile_Reihenfolge['Zeit'].apply(lambda x: print(x))
        Ventile_Reihenfolge = Ventile_Reihenfolge.fillna('')
        Ventile_Reihenfolge['Dauer'] = Ventile_Reihenfolge['Dauer']#.apply(lambda x: x.lower() == "ja")
        Reihenfolge = Ventile_Reihenfolge.set_index('Zeit').transpose().to_dict(orient='dict')
        self.Ventilablauf = Reihenfolge

    def Schedule(self):
        while self.Betriebszustand:
            self.Ventilsteuerung()
            time.sleep(1)

    def Ventilsteuerung(self):
        Uhrzeit = time.strftime("%H-%M")
        if (Uhrzeit in self.Ventilablauf) & ~self.MessungFertig:
            Aktuelle_Ventil = self.Ventilablauf[Uhrzeit]['Ventil']
            self.Logandprint('Zusatz fertig')
            self.MessungFertig = True
            if Aktuelle_Ventil != '':
                self.Unipi.openVentil(Aktuelle_Ventil, CloseAll=False)
                time.sleep(self.Ventilablauf[Uhrzeit]['Dauer'])
                self.Unipi.closeVentil(Aktuelle_Ventil)
        elif Uhrzeit not in self.Ventilablauf:
            self.MessungFertig = False


if __name__ == '__main__':
    Steuerung = ZusatzSteuerung()
    Steuerung.Starten()