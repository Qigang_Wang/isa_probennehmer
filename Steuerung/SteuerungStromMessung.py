import pandas as pd
import threading
import time
from Unipi import UniPiClient
from Datenbank import dbConnection
from Settings import Settings
import math
from Mailservice import sendEmail

class StromMessung(object):
    Thread_Strommessungsteuerung = None
    Betriebszustand = False
    Unipi = None
    Setting = None
    Log = None
    Iteration = 1
    Messungszustand = {"Fehler": False,
                       'Mittelwert': 0}

    def __init__(self):
        pass

    def Logandprint(self, log: str):
        self.Log.info(log)

    def Starten(self):
        if not self.Betriebszustand:
            self.Setting = Settings().Setting
            self.Iteration = self.Setting['Strommessung']['Intervall']
            self.Unipi = UniPiClient()
            self.Betriebszustand = True
            self.Thread_Strommessungsteuerung = threading.Thread(target=self.Schedule, daemon=True)
            self.Thread_Strommessungsteuerung.start()
            self.Logandprint('Die Strommessung wird gestartet!')
        else:
            self.Logandprint('Die Strommessung läuft noch!')

    def Beenden(self):
        if self.Betriebszustand:
            self.Betriebszustand = False
            self.Unipi.Beenden()
            self.Logandprint('Strommessung beendet gerade')
        else:
            self.Logandprint('Strommessung läuft schon nicht mehr')

    def Schedule(self):
        Datenbankverbindung = dbConnection(Datenbank='Strommessdaten.db')
        i = 1
        Ergebnis = {'Strom_Zulauf_1': 0,
                    'Strom_Zulauf_2': 0,
                    'Strom_Rueklauf_1': 0,
                    'Strom_Rueklauf_2': 0}

        while time.strftime("%S") != '59':
            time.sleep(0.1)

        StartTime = math.floor(time.time())

        while self.Betriebszustand:
            try:
                while time.time() < StartTime + 1:
                    pass

                StartTime = math.floor(StartTime + 1)
                if i == 1:
                    Ergebnis['MesszeitStart'] = time.strftime("%Y-%m-%d %H:%M:%S")
                Messung = self.StromMessung()
                Ergebnis['Strom_Zulauf_1'] += Messung['Strom_Zulauf_1']
                Ergebnis['Strom_Zulauf_2'] += Messung['Strom_Zulauf_2']
                Ergebnis['Strom_Rueklauf_1'] += Messung['Strom_Rueklauf_1']
                Ergebnis['Strom_Rueklauf_2'] += Messung['Strom_Rueklauf_2']
                if i == self.Iteration:
                    Ergebnis['Strom_Zulauf_1'] = Ergebnis['Strom_Zulauf_1'] / self.Iteration
                    Ergebnis['Strom_Zulauf_2'] = Ergebnis['Strom_Zulauf_2'] / self.Iteration
                    Ergebnis['Strom_Rueklauf_1'] = Ergebnis['Strom_Rueklauf_1'] / self.Iteration
                    Ergebnis['Strom_Rueklauf_2'] = Ergebnis['Strom_Rueklauf_2'] / self.Iteration
                    Ergebnis['MesszeitEnde'] = time.strftime("%Y-%m-%d %H:%M:%S")
                    if Ergebnis['Strom_Zulauf_1'] == self.Messungszustand['Mittelwert']:
                        if not self.Messungszustand['Fehler']:
                            self.Messungszustand['Fehler'] = True
                            sendEmail(Inhalt=' - Fehler bei der Stommessung')
                    else:
                        self.Messungszustand['Fehler'] = False
                        self.Messungszustand['Mittelwert'] = Ergebnis['Strom_Zulauf_1']

                    Datenbankverbindung.Save_Strom_Messdaten(Ergebnis)
                    # self.Logandprint('Neue Strommessung')
                    Ergebnis = {'Strom_Zulauf_1': 0,
                                'Strom_Zulauf_2': 0,
                                'Strom_Rueklauf_1': 0,
                                'Strom_Rueklauf_2': 0}
                    i = 1
                else:
                    i += 1
            except:
                self.Logandprint('Feler in der Strommessung')

    def StromMessung(self):
        Strom_Zulauf_2 = self.Unipi.getAnalogInput('2.01')
        Strom_Zulauf_1 = self.Unipi.getAnalogInput('2.02')
        Strom_Rueklauf_2 = self.Unipi.getAnalogInput('2.03')
        Strom_Rueklauf_1 = self.Unipi.getAnalogInput('2.04')
        Ergebnis = {'Strom_Zulauf_1': Strom_Zulauf_1,
                    'Strom_Zulauf_2': Strom_Zulauf_2,
                    'Strom_Rueklauf_1': Strom_Rueklauf_1,
                    'Strom_Rueklauf_2': Strom_Rueklauf_2}
        return Ergebnis


if __name__ == '__main__':
    Steuerung = StromMessung()
    Steuerung.Starten()
    time.sleep(5)
    Steuerung.Beenden()
    time.sleep(3)
