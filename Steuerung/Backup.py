import time
import owncloud
import shutil
import os
from Settings import Settings
import threading
import schedule
import sys


class Backup:
    Betriebszustand = False
    Thread_Ventilsteuerung = None
    Backup_Periode = None
    Sciebo_Link = None
    Backup_Zeit = None
    Log = None

    def __init__(self):
        pass

    def Logandprint(self, log: str):
        if sys.platform == 'win32':
            print(log)
        else:
            try:
                self.Log.info(log)
            except:
                pass

    def Starten(self):
        if not self.Betriebszustand:
            Setting = Settings().Setting
            self.Sciebo_Link = Setting['Backup']['ScieboLink']
            self.Backup_Periode = Setting['Backup']['Backup_Periode']
            self.Backup_Zeit = Setting['Backup']['Backup_Zeit']
            self.Betriebszustand = True
            self.Backup_Aller_Datenbank()
            self.Thread_Ventilsteuerung = threading.Thread(target=self.Backup_Schedule, daemon=True)
            self.Thread_Ventilsteuerung.start()
            self.Logandprint('Backup gestartet.')
        else:
            pass

    def Beenden(self):
        if self.Betriebszustand:
            self.Betriebszustand = False
            self.Logandprint('Backup gestopt.')

    def Backup_Schedule(self):
        schedule.every(self.Backup_Periode).days.at(self.Backup_Zeit).do(self.Backup_Aller_Datenbank)
        while self.Betriebszustand:
            schedule.run_pending()
            time.sleep(100)

    def Backup_Aller_Datenbank(self):
        try:
            self.Setting = Settings().Setting
            DatenbankName = 'Messdaten_' + str(self.Setting['Backup']['MessdatenNum']) + '.db'
            self.Datenbank_backup(dbDatei=DatenbankName)
            DatenbankName = 'Strommessdaten_' + str(self.Setting['Backup']['StromMessdatenNum']) + '.db'
            self.Datenbank_backup(dbDatei=DatenbankName)
        except Exception as e:
            print(f"Backup Error: {e}")

    def Datenbank_backup(self, dbDatei='Messdaten.db'):
        try:
            Lokale_Zeit = time.localtime()
            Filename = f'{dbDatei[:-3]}_backup_{Lokale_Zeit.tm_year}_{Lokale_Zeit.tm_mon}_' \
                       f'{Lokale_Zeit.tm_mday}_{Lokale_Zeit.tm_hour}_{Lokale_Zeit.tm_min}_{Lokale_Zeit.tm_sec}.db'
            Ordner = os.getcwd().replace('\\', '/')
            shutil.copyfile(os.path.join(Ordner, dbDatei), os.path.join(Ordner, Filename))
            oc = owncloud.Client.from_public_link(self.Sciebo_Link)
            file_name = os.path.join(Ordner, Filename)
            destination = '/' + os.path.basename(file_name)
            oc.put_file(destination, file_name, chunked=False)
            os.remove(os.path.join(Ordner, Filename))
        except Exception as e:
            print(f"Backup Error: Hochladen nicht erfolgreich - {e}")


if __name__ == "__main__":
    B = Backup()
    B.Starten()
