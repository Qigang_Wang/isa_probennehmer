import json


class Settings(object):
    FileName = "Settings.json"
    Setting = {'Tribox': {'ip': "192.168.178.15",
                          'port': 502},
               "UniPi": {'ip': "192.168.178.75"},
               "Backup": {'ScieboLink': "https://rwth-aachen.sciebo.de/s/V0kVRRL8JvdPdZi",
                          'Backup_Periode': 2,
                          'Backup_Zeit': '00:00',
                          'DBmaxSize': 50,
                          'MessdatenNum': 1,
                          'StromMessdatenNum': 1},
               'Strommessung': {'Intervall': 10}
               }

    def __init__(self):
        self.save_settings()

    def save_settings(self):
        File = open(self.FileName, "w")
        File.write(json.dumps(self.Setting))
        File.close()


if __name__ == "__main__":
    print(Settings().Setting)
