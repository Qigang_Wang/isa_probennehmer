import sqlite3
import time
from threading import Lock
from xlsxwriter.workbook import Workbook
from Settings import Settings
import os
from Backup import Backup
from threading import Thread


class dbConnection:
    TableName = None
    Jahr = None
    dbName = 'Messdaten.db'

    def __init__(self, Datenbank='Messdaten.db'):
        self.Setting = Settings()
        # print(self.Setting.Setting)
        if Datenbank == 'Messdaten.db':
            DatenbankName = 'Messdaten_'+str(self.Setting.Setting['Backup']['MessdatenNum'])+'.db'
            try:
                if os.stat(DatenbankName).st_size / 1024 / 1024 > self.Setting.Setting['Backup']['DBmaxSize']:
                    Thread(target=self.BackupDB, args=(DatenbankName,)).start()
                    self.Setting.Setting['Backup']['MessdatenNum'] = self.Setting.Setting['Backup']['MessdatenNum'] + 1
                    self.Setting.save_settings()
                    DatenbankName = 'Messdaten_'+str(self.Setting.Setting['Backup']['MessdatenNum'])+'.db'
            except:
                print('Error')
        elif Datenbank == 'Strommessdaten.db':
            DatenbankName = 'Strommessdaten_'+str(self.Setting.Setting['Backup']['StromMessdatenNum'])+'.db'
            try:
                if os.stat(DatenbankName).st_size / 1024 / 1024 > self.Setting.Setting['Backup']['DBmaxSize']:
                    Thread(target=self.BackupDB, args=(DatenbankName,)).start()
                    self.Setting.Setting['Backup']['StromMessdatenNum'] = self.Setting.Setting['Backup']['StromMessdatenNum'] + 1
                    self.Setting.save_settings()
                    DatenbankName = 'Strommessdaten_'+str(self.Setting.Setting['Backup']['StromMessdatenNum']+1)+'.db'
            except:
                print('Error')


        self.dbName = DatenbankName
        self.generateTableName()
        self.conn = sqlite3.connect(self.dbName)
        self.dbLock = Lock()
        if not self.existTable():
            self.createTable()
    def BackupDB(self, FileName):
        BackupServer = Backup()
        BackupServer.Datenbank_backup(dbDatei=FileName)
        pass

    def RunBefehle(self, Befehle):
        self.dbLock.acquire()
        c = self.conn.cursor()
        c.execute(Befehle)
        Ergebnis = c.fetchall()
        self.conn.commit()
        c.close()
        self.dbLock.release()
        return Ergebnis

    def existTable(self):
        Befehl = f"SELECT COUNT(*) FROM sqlite_master where type ='table' and name ='{self.TableName}'"
        Ergebnis = self.RunBefehle(Befehl)
        Flag = False
        if Ergebnis[0][0]:
            Flag = True
        return Flag

    def generateTableName(self):
        self.TableName = 'Jahr' + time.strftime("%Y")
        self.Jahr = time.strftime("%Y")

    def createTable(self, Mod='Steuerung'):

        if 'Messdaten_' in self.dbName:
            Befehl = '''CREATE TABLE "{TabelName}" (
                        "MessungID" INTEGER NOT NULL,
                        "Messzeit" DATETIME NULL,
                        "Ventil" CHAR(8) NULL,
                        "NO3_N" REAL NULL,
                        "NO2_N" REAL NULL,
                        "CSBeq" REAL NULL,
                        "TSSeq" REAL NULL,
                        "SAK254" REAL NULL,
                        "ABS360" REAL NULL,
                        "ABS210" REAL NULL,
                        "Fit_Error" REAL NULL,
                        "ABS254" REAL NULL,
                        "NO3" REAL NULL,
                        "NO2" REAL NULL,
                        "COD_SACeq" REAL NULL,
                        "TOC_SACeq" REAL NULL,
                        "DOC_SACeq" REAL NULL,
                        "SQI" REAL NULL,
                        "UVT254" REAL NULL,
                        "Kurve" TEXT NULL,
                        PRIMARY KEY ("MessungID")
                    )
                    ;'''.format(TabelName=self.TableName)
        elif 'Strommessdaten_' in self.dbName:
            Befehl = '''CREATE TABLE "{TabelName}" (
                        "MessungID" INTEGER NOT NULL,
                        "MesszeitStart" DATETIME NULL,
                        "MesszeitEnde" DATETIME NULL,
                        "Strom_Zulauf_1" REAL NULL,
                        "Strom_Zulauf_2" REAL NULL,
                        "Strom_Rueklauf_1" REAL NULL,
                        "Strom_Rueklauf_2" REAL NULL,
                        PRIMARY KEY ("MessungID")
                    )
                    ;'''.format(TabelName=self.TableName)

        Ergebnis = self.RunBefehle(Befehl)

    def Save_Messdaten(self, Messdaten: dict):
        Keys = ""
        Values = ""
        for key in Messdaten:
            Keys = Keys + "'{key}', ".format(key=key)
            Values = Values + "'{Value}', ".format(Value=Messdaten[key]['Wert'])
        Values = Values.replace("'Wavelength'", '"Wavelength"')
        Values = Values.replace("'Absorption'", '"Absorption"')
        Befehl = 'INSERT INTO "{TabelName}" ({Keys}) VALUES ({Values});'.format(
            TabelName=self.TableName, Keys=Keys.rstrip(', '), Values=Values.rstrip(', '))
        self.RunBefehle(Befehl)

    def Save_Strom_Messdaten(self, Messdaten: dict):
        Keys = ""
        Values = ""
        for key in Messdaten:
            Keys = Keys + "'{key}', ".format(key=key)
            Values = Values + "'{Value}', ".format(Value=Messdaten[key])
        Befehl = 'INSERT INTO "{TabelName}" ({Keys}) VALUES ({Values});'.format(
            TabelName=self.TableName, Keys=Keys.rstrip(', '), Values=Values.rstrip(', '))
        self.RunBefehle(Befehl)

    def save_as_excel(self):
        self.ExcelName = self.TableName + '_Messdaten.xlsx'
        workbook = Workbook(self.ExcelName)
        worksheet = workbook.add_worksheet("Messdaten")
        conn = sqlite3.connect(self.dbName)
        c = conn.cursor()
        mysel = c.execute("select * from {}".format(self.TableName))

        worksheet.write_row(0, 0, ['MessungID', 'Messzeit', "Ventil", "NO3_N",
                                   "NO2_N", "CSBeq", "TSSeq", "SAK254", "ABS360", "ABS210", "Fit_Error", "ABS254",
                                   "NO3", "NO2", "COD_SACeq", "TOC_SACeq", "DOC_SACeq", "SQI", "UVT254", "Kurve", ])
        for i, row in enumerate(mysel):
            for j, value in enumerate(row[0:19]):
                worksheet.write(i + 1, j, value)
        workbook.close()

    def save_Strommessadaten_as_excel(self):
        self.ExcelName = self.TableName + '_StromMessdaten.xlsx'
        workbook = Workbook(self.ExcelName)
        worksheet = workbook.add_worksheet("Messdaten")
        conn = sqlite3.connect(self.dbName)
        c = conn.cursor()
        mysel = c.execute("select * from {}".format(self.TableName))

        worksheet.write_row(0, 0, ['MessungID', 'Messzeit', "Strom_Zulauf_1", "Strom_Zulauf_2",
                                   "Strom_Rueklauf_1", "Strom_Rueklauf_2"])
        for i, row in enumerate(mysel):
            for j, value in enumerate(row[0:6]):
                worksheet.write(i + 1, j, value)
        workbook.close()


if __name__ == "__main__":
    dbCon = dbConnection(Datenbank='Strommessdaten.db')
    print(dbCon.save_Strommessadaten_as_excel())
