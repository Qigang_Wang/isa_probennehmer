import http.client
import json
import time
from Settings import Settings
import logging


# https://evok-14.api-docs.io/1.11/gmweevhniahi2ek6w/introduction

class UniPiClient:
    UniPi_ip = ''
    Ventilzuordnung = {'E1': {'Type': 'DO', 'Address': '1.04', 'Observer': '2.04'},
                       'E2': {'Type': 'DO', 'Address': '1.03', 'Observer': '2.03'},
                       'K1': {'Type': 'DO', 'Address': '1.02', 'Observer': '2.02'},
                       'K2': {'Type': 'DO', 'Address': '1.01', 'Observer': '2.01'},
                       'S1': {'Type': 'RO', 'Address': '2.02', 'Observer': '2.05'},
                       'Z1': {'Type': 'RO', 'Address': '2.04', 'Observer': '2.06'}
                       }
    HTTP_Verbindung = None
    OutputType = None
    Log = None

    def __init__(self):

        self.Initialisierung()

    def Initialisierung(self):
        Setting = Settings().Setting
        self.UniPi_ip = Setting['UniPi']['ip']
        self.HTTP_Verbindung = http.client.HTTPConnection(self.UniPi_ip + ":8080")
        self.OutputType = {'DO': self.setDigitalOutput,
                           'RO': self.setRelayOutput}
        self.Log = logging

    def Beenden(self):
        self.HTTP_Verbindung.close()

    def getParameters(self, Type, Addresse, Payload="{}"):
        self.HTTP_Verbindung.request("GET", "/json/" + Type + "/" + Addresse, Payload)
        res = self.HTTP_Verbindung.getresponse()
        Ergebnis = json.loads(res.read().decode("utf-8"))
        return Ergebnis

    def setParameters(self, Type, Addresse, value):
        payload = '{"value":"' + value + '"}'
        headers = {'content-type': "application/json"}
        self.HTTP_Verbindung.request("POST", "/json/" + Type + "/" + Addresse, payload, headers)
        res = self.HTTP_Verbindung.getresponse()
        Ergebnis = json.loads(res.read().decode("utf-8"))
        return Ergebnis

    def getDigitalInput(self, Addresse):
        Ergebnis = self.getParameters(Type="input", Addresse=Addresse.replace(".", "_"))
        if Ergebnis['status'] == 'success':
            status = True
        else:
            status = False
        return status

    def getDigitalOutput(self, Addresse):
        Ergebnis = self.getParameters(Type="output", Addresse=str(Addresse).replace(".", "_"))
        return Ergebnis['data']['value']

    def setDigitalOutput(self, Addresse, value):
        Ergebnis = self.setParameters(Type="output", Addresse=str(Addresse).replace(".", "_"), value=str(value))
        # print(Ergebnis)
        if Ergebnis['status'] == 'success':
            status = True
        else:
            status = False
        return status

    def setRelayOutput(self, Addresse, value):
        Ergebnis = self.setParameters(Type="relay", Addresse=str(Addresse).replace(".", "_"), value=str(value))
        if Ergebnis['status'] == 'success':
            status = True
        else:
            status = False
        return status

    def getAnalogInput(self, Addresse):
        Ergebnis = self.getParameters(Type="analoginput", Addresse=Addresse.replace(".", "_"))
        if Ergebnis['status'] == 'success':
            value = Ergebnis['data']['value']
        else:
            value = -1
        return value

    def closeVentil(self, Ventil):
        # self.closeVentil('E1')
        if Ventil == '':
            return None
        VentilParameter = self.Ventilzuordnung[Ventil]
        self.OutputType[VentilParameter['Type']](Addresse=VentilParameter['Address'], value=0)

    def closeallVentile(self):
        # Unipi.closeallVentile()
        for key in self.Ventilzuordnung:
            if key != 'Z1':
                self.closeVentil(key)

    def openVentil(self, Ventil, CloseAll=True):
        if Ventil == '':
            return False
        if CloseAll:
            self.closeallVentile()
        VentilParameter = self.Ventilzuordnung[Ventil]
        self.OutputType[VentilParameter['Type']](Addresse=VentilParameter['Address'], value=1)
        Ergebnis = self.getDigitalInput(Addresse=VentilParameter['Observer'])
        return Ergebnis

    def StartProbenehmer(self, Ventil):
        self.openVentil("E1", CloseAll=False)
        self.openVentil("E2", CloseAll=False)
        self.openVentil("K1", CloseAll=False)
        self.openVentil("K2", CloseAll=False)

        time.sleep(10)
        if not self.openVentil(Ventil):
            self.Log.error(f'Ventil {Ventil} könnte nicht richtig geöffnet')
        time.sleep(1)
        self.setRelayOutput(Addresse=2.01, value=1)
        time.sleep(1)
        self.setRelayOutput(Addresse=2.01, value=0)
        self.Log.info(f'Ventilzustand: '
                      f'E1-{str(int(self.getDigitalInput(Addresse=self.Ventilzuordnung["E1"]["Observer"])))}_'
                      f'E2-{str(int(self.getDigitalInput(Addresse=self.Ventilzuordnung["E2"]["Observer"])))}_'
                      f'K1-{str(int(self.getDigitalInput(Addresse=self.Ventilzuordnung["K1"]["Observer"])))}_'
                      f'K2-{str(int(self.getDigitalInput(Addresse=self.Ventilzuordnung["K2"]["Observer"])))}')
        # if self.openVentil(Ventil):
        #     self.setRelayOutput(Addresse=2.01, value=1)
        #     time.sleep(1)
        #     if not Unipi.getDigitalOutput(Addresse='1.01'):
        #         self.Log.error('Ventil "Probenehmer" wird nicht richtig eingeschalt')
        #     self.setRelayOutput(Addresse=2.01, value=0)
        # else:
        #     self.Log.error(f'Ventil  {Ventil} wird nicht richtig eingeschaltet. Kiene Messung wird durchgeführt.')


if __name__ == "__main__":
    Unipi = UniPiClient()
    # print(Unipi.getAnalogInput('2.01'))
    # print(Unipi.setDigitalOutput(Addresse=1.03, value=1))
    # print(Unipi.getDigitalOutput(Addresse='1.02'))
    print(Unipi.getDigitalOutput(Addresse='1.01'))
