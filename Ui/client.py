from socket import *
import time
import json
import os
import math

from Settings import Settings


class socket_client:
    # serve_ip = "192.168.178.75"
    serve_ip = "127.0.0.1"
    serve_port = 8000  # 端口，比如8000
    tcp_socket = None
    TCPLength = 1024

    def __init__(self):
        # self.Setting = Settings()
        self.create_TCP_Verbindund()

    def create_TCP_Verbindund(self):
        # create a TCP Connection
        # self.Setting = Settings()
        # self.serve_ip = self.Setting.Setting['UniPi']['ip']
        self.tcp_socket = socket(AF_INET, SOCK_STREAM)
        self.tcp_socket.connect((self.serve_ip, self.serve_port))

    def TCP_Nachricht_senden(self, conn: socket = None, Nachricht: str = ''):
        NachrichtLength = len(Nachricht)

        if NachrichtLength > self.TCPLength:
            Anzahl = math.ceil(NachrichtLength / self.TCPLength)
            NachrichtCodierung = Nachricht.encode("utf-8")
            conn.send('**Start**'.encode('utf-8'))
            for i in range(Anzahl):
                conn.send(NachrichtCodierung[0 + i * self.TCPLength:self.TCPLength + i * self.TCPLength])
            conn.send('**Stop**'.encode('utf-8'))
        else:
            conn.send(Nachricht.ljust(self.TCPLength).encode("utf-8"))


    def TCP_Befehl_senden(self, Befehl: dict = ''):
        self.TCP_Nachricht_senden(self.tcp_socket, json.dumps(Befehl))


    def TCP_Nachricht_empfangen(self, conn: socket = None):
        Nachrichten = conn.recv(self.TCPLength).decode('utf-8')

        if '**Start**' in Nachrichten:
            # Shortmsg = from_server_msg.replace(' ', '')
            # print(Shortmsg)
            # LengthInformation = json.loads(Shortmsg)
            # Anzhal = LengthInformation['Anzahl']
            # Nachrichten = ''
            # for i in range(Anzhal):
            #     Nachrichten = Nachrichten + conn.recv(self.TCPLength).decode('utf-8').replace(' ', '')
            while '**Stop**' not in Nachrichten:
                Nachrichten = Nachrichten + conn.recv(self.TCPLength).decode('utf-8')

            Start = Nachrichten.find('**Start**')
            Stop = Nachrichten.find('**Stop**')
            Ergebnis = Nachrichten[Start+9:Stop]
        else:
            Ergebnis = Nachrichten
        return Ergebnis

    def TCP_Close(self):
        self.tcp_socket.close()

    def DateiEmpfangen(self):
        Header = json.loads(self.TCP_Nachricht_empfangen(self.tcp_socket))
        with open(r'%s\%s' % (os.getcwd(), Header['filename']), 'wb') as f:
            recv_size = 0
            while recv_size < Header['filesize']:
                line = self.tcp_socket.recv(self.TCPLength)
                f.write(line)
                recv_size += len(line)
                # print(recv_size)

    def Datei_senden(self, conn: socket = None, Filename: str = "Reihenfolge.xlsx"):
        Header_Dic = {'filename': Filename,
                      'filesize': os.path.getsize(Filename)}
        self.TCP_Nachricht_senden(self.tcp_socket, json.dumps(Header_Dic))
        time.sleep(0.5)
        with open('%s/%s' % (os.getcwd(), Filename), 'rb') as f:
            for line in f:

                # print(len(line))
                # self.TCP_Nachricht_senden(self.tcp_socket, line.decode('utf-8'))
                conn.send(line)


if __name__ == "__main__":
    Verbindung = socket_client()
    # Beefhle
    MessungStarten = {'Befehl': 'MessungStarten'}
    MessungBeenden = {'Befehl': 'MessungBeenden'}
    BackupStarten = {'Befehl': 'BackupStarten'}
    BackupBeenden = {'Befehl': 'BackupBeenden'}
    getMessdaten = {'Befehl': 'getMessdaten'}

    S_Befehl = getMessdaten
    Verbindung.TCP_Befehl_senden(S_Befehl)
    Verbindung.TCP_Nachricht_empfangen()

    # print(Verbindung.TCP_Nachricht_empfangenLong())
    time.sleep(0.1)
    Verbindung.TCP_Close()
