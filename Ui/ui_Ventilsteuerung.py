# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'Ventilsteuerung.ui'
##
## Created by: Qt User Interface Compiler version 6.4.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QGridLayout, QGroupBox,
    QHBoxLayout, QLabel, QLineEdit, QMainWindow,
    QPushButton, QSizePolicy, QSpacerItem, QStatusBar,
    QTabWidget, QTextEdit, QVBoxLayout, QWidget)

class Ui_Ventilsteuerung(object):
    def setupUi(self, Ventilsteuerung):
        if not Ventilsteuerung.objectName():
            Ventilsteuerung.setObjectName(u"Ventilsteuerung")
        Ventilsteuerung.resize(746, 550)
        Ventilsteuerung.setMinimumSize(QSize(600, 550))
        Ventilsteuerung.setMaximumSize(QSize(4000, 4000))
        self.centralwidget = QWidget(Ventilsteuerung)
        self.centralwidget.setObjectName(u"centralwidget")
        self.horizontalLayout = QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.tabWidget = QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tabWidget.setEnabled(True)
        self.tabWidget.setMinimumSize(QSize(0, 400))
        self.tabWidget.setTabPosition(QTabWidget.North)
        self.tabWidget.setTabShape(QTabWidget.Rounded)
        self.Seite_Messung = QWidget()
        self.Seite_Messung.setObjectName(u"Seite_Messung")
        self.horizontalLayout_5 = QHBoxLayout(self.Seite_Messung)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.MessungText = QTextEdit(self.Seite_Messung)
        self.MessungText.setObjectName(u"MessungText")
        self.MessungText.setMinimumSize(QSize(0, 0))
        self.MessungText.setReadOnly(True)

        self.horizontalLayout_4.addWidget(self.MessungText)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.Messdate_Abholen = QPushButton(self.Seite_Messung)
        self.Messdate_Abholen.setObjectName(u"Messdate_Abholen")
        self.Messdate_Abholen.setMinimumSize(QSize(26, 0))

        self.verticalLayout.addWidget(self.Messdate_Abholen)

        self.line = QFrame(self.Seite_Messung)
        self.line.setObjectName(u"line")
        self.line.setMinimumSize(QSize(0, 0))
        self.line.setBaseSize(QSize(0, 0))
        self.line.setFrameShape(QFrame.HLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.verticalLayout.addWidget(self.line)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.label_6 = QLabel(self.Seite_Messung)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setMaximumSize(QSize(70, 20))

        self.horizontalLayout_6.addWidget(self.label_6)

        self.Messung_Status = QPushButton(self.Seite_Messung)
        self.Messung_Status.setObjectName(u"Messung_Status")
        self.Messung_Status.setEnabled(False)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.Messung_Status.sizePolicy().hasHeightForWidth())
        self.Messung_Status.setSizePolicy(sizePolicy)
        self.Messung_Status.setMinimumSize(QSize(20, 20))
        self.Messung_Status.setMaximumSize(QSize(20, 20))

        self.horizontalLayout_6.addWidget(self.Messung_Status)


        self.verticalLayout.addLayout(self.horizontalLayout_6)

        self.horizontalLayout_8 = QHBoxLayout()
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.label_9 = QLabel(self.Seite_Messung)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setMaximumSize(QSize(70, 20))

        self.horizontalLayout_8.addWidget(self.label_9)

        self.AktuellVentil = QLabel(self.Seite_Messung)
        self.AktuellVentil.setObjectName(u"AktuellVentil")
        self.AktuellVentil.setMaximumSize(QSize(20, 16777215))

        self.horizontalLayout_8.addWidget(self.AktuellVentil)


        self.verticalLayout.addLayout(self.horizontalLayout_8)

        self.Messung_Starten = QPushButton(self.Seite_Messung)
        self.Messung_Starten.setObjectName(u"Messung_Starten")
        self.Messung_Starten.setMinimumSize(QSize(26, 0))

        self.verticalLayout.addWidget(self.Messung_Starten)

        self.Messung_Beenden = QPushButton(self.Seite_Messung)
        self.Messung_Beenden.setObjectName(u"Messung_Beenden")
        self.Messung_Beenden.setMinimumSize(QSize(26, 0))

        self.verticalLayout.addWidget(self.Messung_Beenden)

        self.line_2 = QFrame(self.Seite_Messung)
        self.line_2.setObjectName(u"line_2")
        self.line_2.setFrameShape(QFrame.HLine)
        self.line_2.setFrameShadow(QFrame.Sunken)

        self.verticalLayout.addWidget(self.line_2)

        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.label_8 = QLabel(self.Seite_Messung)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setMaximumSize(QSize(70, 20))

        self.horizontalLayout_7.addWidget(self.label_8)

        self.Backup_Status = QPushButton(self.Seite_Messung)
        self.Backup_Status.setObjectName(u"Backup_Status")
        self.Backup_Status.setEnabled(False)
        sizePolicy.setHeightForWidth(self.Backup_Status.sizePolicy().hasHeightForWidth())
        self.Backup_Status.setSizePolicy(sizePolicy)
        self.Backup_Status.setMinimumSize(QSize(20, 20))
        self.Backup_Status.setMaximumSize(QSize(20, 20))

        self.horizontalLayout_7.addWidget(self.Backup_Status)


        self.verticalLayout.addLayout(self.horizontalLayout_7)

        self.Backup_Sofort = QPushButton(self.Seite_Messung)
        self.Backup_Sofort.setObjectName(u"Backup_Sofort")
        self.Backup_Sofort.setMinimumSize(QSize(26, 0))

        self.verticalLayout.addWidget(self.Backup_Sofort)

        self.Backup_Starten = QPushButton(self.Seite_Messung)
        self.Backup_Starten.setObjectName(u"Backup_Starten")
        self.Backup_Starten.setMinimumSize(QSize(26, 0))

        self.verticalLayout.addWidget(self.Backup_Starten)

        self.Backup_Beenden = QPushButton(self.Seite_Messung)
        self.Backup_Beenden.setObjectName(u"Backup_Beenden")
        self.Backup_Beenden.setMinimumSize(QSize(26, 0))

        self.verticalLayout.addWidget(self.Backup_Beenden)

        self.line_3 = QFrame(self.Seite_Messung)
        self.line_3.setObjectName(u"line_3")
        self.line_3.setFrameShape(QFrame.HLine)
        self.line_3.setFrameShadow(QFrame.Sunken)

        self.verticalLayout.addWidget(self.line_3)

        self.Log_Exportieren = QPushButton(self.Seite_Messung)
        self.Log_Exportieren.setObjectName(u"Log_Exportieren")
        self.Log_Exportieren.setMinimumSize(QSize(26, 0))

        self.verticalLayout.addWidget(self.Log_Exportieren)

        self.line_4 = QFrame(self.Seite_Messung)
        self.line_4.setObjectName(u"line_4")
        self.line_4.setFrameShape(QFrame.HLine)
        self.line_4.setFrameShadow(QFrame.Sunken)

        self.verticalLayout.addWidget(self.line_4)

        self.pushButton = QPushButton(self.Seite_Messung)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setMinimumSize(QSize(26, 0))

        self.verticalLayout.addWidget(self.pushButton)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_2)


        self.horizontalLayout_4.addLayout(self.verticalLayout)


        self.horizontalLayout_5.addLayout(self.horizontalLayout_4)

        self.tabWidget.addTab(self.Seite_Messung, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.verticalLayout_3 = QVBoxLayout(self.tab_2)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalSpacer_9 = QSpacerItem(20, 32, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer_9)

        self.groupBox_4 = QGroupBox(self.tab_2)
        self.groupBox_4.setObjectName(u"groupBox_4")
        self.groupBox_4.setMaximumSize(QSize(20000, 20000))
        self.verticalLayout_4 = QVBoxLayout(self.groupBox_4)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalSpacer_3 = QSpacerItem(20, 38, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_4.addItem(self.verticalSpacer_3)

        self.horizontalLayout_10 = QHBoxLayout()
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.gridLayout_4 = QGridLayout()
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.label_12 = QLabel(self.groupBox_4)
        self.label_12.setObjectName(u"label_12")

        self.gridLayout_4.addWidget(self.label_12, 0, 0, 1, 1)

        self.Zusatz_Status = QPushButton(self.groupBox_4)
        self.Zusatz_Status.setObjectName(u"Zusatz_Status")
        self.Zusatz_Status.setEnabled(False)
        sizePolicy.setHeightForWidth(self.Zusatz_Status.sizePolicy().hasHeightForWidth())
        self.Zusatz_Status.setSizePolicy(sizePolicy)
        self.Zusatz_Status.setMinimumSize(QSize(20, 20))
        self.Zusatz_Status.setMaximumSize(QSize(20, 20))

        self.gridLayout_4.addWidget(self.Zusatz_Status, 0, 1, 1, 1)

        self.Zusatz_change = QPushButton(self.groupBox_4)
        self.Zusatz_change.setObjectName(u"Zusatz_change")

        self.gridLayout_4.addWidget(self.Zusatz_change, 1, 0, 1, 2)

        self.Zusatz_change_2 = QPushButton(self.groupBox_4)
        self.Zusatz_change_2.setObjectName(u"Zusatz_change_2")

        self.gridLayout_4.addWidget(self.Zusatz_change_2, 1, 2, 1, 1)


        self.horizontalLayout_10.addLayout(self.gridLayout_4)

        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_10.addItem(self.horizontalSpacer_4)


        self.verticalLayout_4.addLayout(self.horizontalLayout_10)

        self.verticalSpacer_4 = QSpacerItem(20, 38, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_4.addItem(self.verticalSpacer_4)


        self.verticalLayout_3.addWidget(self.groupBox_4)

        self.verticalSpacer_7 = QSpacerItem(20, 31, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer_7)

        self.groupBox_6 = QGroupBox(self.tab_2)
        self.groupBox_6.setObjectName(u"groupBox_6")
        self.groupBox_6.setMaximumSize(QSize(20000, 20000))
        self.verticalLayout_5 = QVBoxLayout(self.groupBox_6)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalSpacer_5 = QSpacerItem(20, 38, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_5.addItem(self.verticalSpacer_5)

        self.horizontalLayout_9 = QHBoxLayout()
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.gridLayout_6 = QGridLayout()
        self.gridLayout_6.setObjectName(u"gridLayout_6")
        self.Strommessen_Stop = QPushButton(self.groupBox_6)
        self.Strommessen_Stop.setObjectName(u"Strommessen_Stop")

        self.gridLayout_6.addWidget(self.Strommessen_Stop, 1, 2, 1, 1)

        self.Strommessen_Status = QPushButton(self.groupBox_6)
        self.Strommessen_Status.setObjectName(u"Strommessen_Status")
        self.Strommessen_Status.setEnabled(False)
        sizePolicy.setHeightForWidth(self.Strommessen_Status.sizePolicy().hasHeightForWidth())
        self.Strommessen_Status.setSizePolicy(sizePolicy)
        self.Strommessen_Status.setMinimumSize(QSize(20, 20))
        self.Strommessen_Status.setMaximumSize(QSize(20, 20))

        self.gridLayout_6.addWidget(self.Strommessen_Status, 0, 1, 1, 1)

        self.Strommessen_Start = QPushButton(self.groupBox_6)
        self.Strommessen_Start.setObjectName(u"Strommessen_Start")

        self.gridLayout_6.addWidget(self.Strommessen_Start, 1, 0, 1, 2)

        self.label_13 = QLabel(self.groupBox_6)
        self.label_13.setObjectName(u"label_13")

        self.gridLayout_6.addWidget(self.label_13, 0, 0, 1, 1)


        self.horizontalLayout_9.addLayout(self.gridLayout_6)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_9.addItem(self.horizontalSpacer_3)


        self.verticalLayout_5.addLayout(self.horizontalLayout_9)

        self.verticalSpacer_6 = QSpacerItem(20, 38, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_5.addItem(self.verticalSpacer_6)


        self.verticalLayout_3.addWidget(self.groupBox_6)

        self.verticalSpacer_8 = QSpacerItem(20, 32, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer_8)

        self.tabWidget.addTab(self.tab_2, "")
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.verticalLayout_2 = QVBoxLayout(self.tab)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.groupBox = QGroupBox(self.tab)
        self.groupBox.setObjectName(u"groupBox")
        self.gridLayout = QGridLayout(self.groupBox)
        self.gridLayout.setObjectName(u"gridLayout")
        self.label = QLabel(self.groupBox)
        self.label.setObjectName(u"label")

        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)

        self.Tribox_ip = QLineEdit(self.groupBox)
        self.Tribox_ip.setObjectName(u"Tribox_ip")

        self.gridLayout.addWidget(self.Tribox_ip, 0, 1, 1, 1)

        self.label_2 = QLabel(self.groupBox)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)

        self.Tribox_port = QLineEdit(self.groupBox)
        self.Tribox_port.setObjectName(u"Tribox_port")

        self.gridLayout.addWidget(self.Tribox_port, 1, 1, 1, 1)


        self.horizontalLayout_2.addWidget(self.groupBox)

        self.groupBox_2 = QGroupBox(self.tab)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.gridLayout_2 = QGridLayout(self.groupBox_2)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.label_3 = QLabel(self.groupBox_2)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout_2.addWidget(self.label_3, 0, 0, 1, 1)

        self.Unipi_ip = QLineEdit(self.groupBox_2)
        self.Unipi_ip.setObjectName(u"Unipi_ip")

        self.gridLayout_2.addWidget(self.Unipi_ip, 0, 1, 1, 1)

        self.label_7 = QLabel(self.groupBox_2)
        self.label_7.setObjectName(u"label_7")

        self.gridLayout_2.addWidget(self.label_7, 1, 0, 1, 1)


        self.horizontalLayout_2.addWidget(self.groupBox_2)


        self.verticalLayout_2.addLayout(self.horizontalLayout_2)

        self.groupBox_3 = QGroupBox(self.tab)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.gridLayout_3 = QGridLayout(self.groupBox_3)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.Sciebolink = QLineEdit(self.groupBox_3)
        self.Sciebolink.setObjectName(u"Sciebolink")

        self.gridLayout_3.addWidget(self.Sciebolink, 0, 1, 1, 1)

        self.label_4 = QLabel(self.groupBox_3)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout_3.addWidget(self.label_4, 0, 0, 1, 1)

        self.label_5 = QLabel(self.groupBox_3)
        self.label_5.setObjectName(u"label_5")

        self.gridLayout_3.addWidget(self.label_5, 1, 0, 1, 1)

        self.Update_Periode = QLineEdit(self.groupBox_3)
        self.Update_Periode.setObjectName(u"Update_Periode")

        self.gridLayout_3.addWidget(self.Update_Periode, 1, 1, 1, 1)

        self.Backup_Zeit1111 = QLabel(self.groupBox_3)
        self.Backup_Zeit1111.setObjectName(u"Backup_Zeit1111")

        self.gridLayout_3.addWidget(self.Backup_Zeit1111, 2, 0, 1, 1)

        self.Backup_Zeit = QLineEdit(self.groupBox_3)
        self.Backup_Zeit.setObjectName(u"Backup_Zeit")

        self.gridLayout_3.addWidget(self.Backup_Zeit, 2, 1, 1, 1)


        self.verticalLayout_2.addWidget(self.groupBox_3)

        self.groupBox_5 = QGroupBox(self.tab)
        self.groupBox_5.setObjectName(u"groupBox_5")
        self.gridLayout_5 = QGridLayout(self.groupBox_5)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.Strommessung_Periode = QLineEdit(self.groupBox_5)
        self.Strommessung_Periode.setObjectName(u"Strommessung_Periode")

        self.gridLayout_5.addWidget(self.Strommessung_Periode, 0, 1, 1, 1)

        self.label_11 = QLabel(self.groupBox_5)
        self.label_11.setObjectName(u"label_11")

        self.gridLayout_5.addWidget(self.label_11, 0, 0, 1, 1)


        self.verticalLayout_2.addWidget(self.groupBox_5)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_2)

        self.Einstellung_Hochladen = QPushButton(self.tab)
        self.Einstellung_Hochladen.setObjectName(u"Einstellung_Hochladen")

        self.horizontalLayout_3.addWidget(self.Einstellung_Hochladen)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer)


        self.verticalLayout_2.addLayout(self.horizontalLayout_3)

        self.tabWidget.addTab(self.tab, "")

        self.horizontalLayout.addWidget(self.tabWidget)

        Ventilsteuerung.setCentralWidget(self.centralwidget)
        self.Infor = QStatusBar(Ventilsteuerung)
        self.Infor.setObjectName(u"Infor")
        Ventilsteuerung.setStatusBar(self.Infor)

        self.retranslateUi(Ventilsteuerung)
        self.Messung_Starten.clicked.connect(Ventilsteuerung.MessungStarten)
        self.Backup_Starten.clicked.connect(Ventilsteuerung.BackupStarten)
        self.Zusatz_change.clicked.connect(Ventilsteuerung.ZusatzVentilStarten)
        self.Log_Exportieren.clicked.connect(Ventilsteuerung.LogHerunterladen)
        self.Messung_Beenden.clicked.connect(Ventilsteuerung.Messungbeenden)
        self.Backup_Sofort.clicked.connect(Ventilsteuerung.BackupSofort)
        self.Zusatz_change_2.clicked.connect(Ventilsteuerung.ZusatzVentilStop)
        self.Einstellung_Hochladen.clicked.connect(Ventilsteuerung.EinstellungHochladen)
        self.pushButton.clicked.connect(Ventilsteuerung.ReihenFolgeHochladen)
        self.Backup_Beenden.clicked.connect(Ventilsteuerung.BackupBeenden)
        self.Messdate_Abholen.clicked.connect(Ventilsteuerung.MessdatenAbholen)
        self.Strommessen_Start.clicked.connect(Ventilsteuerung.StrommessenStarten)
        self.Strommessen_Stop.clicked.connect(Ventilsteuerung.StrommessenStop)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Ventilsteuerung)
    # setupUi

    def retranslateUi(self, Ventilsteuerung):
        Ventilsteuerung.setWindowTitle(QCoreApplication.translate("Ventilsteuerung", u"Ventilsteuerung", None))
        self.Messdate_Abholen.setText(QCoreApplication.translate("Ventilsteuerung", u"Messdaten abholen", None))
        self.label_6.setText(QCoreApplication.translate("Ventilsteuerung", u"Status:", None))
        self.Messung_Status.setText("")
        self.label_9.setText(QCoreApplication.translate("Ventilsteuerung", u"Ventill:", None))
        self.AktuellVentil.setText(QCoreApplication.translate("Ventilsteuerung", u"E2", None))
        self.Messung_Starten.setText(QCoreApplication.translate("Ventilsteuerung", u"Messung starten", None))
        self.Messung_Beenden.setText(QCoreApplication.translate("Ventilsteuerung", u"Messung beenden", None))
        self.label_8.setText(QCoreApplication.translate("Ventilsteuerung", u"Status:", None))
        self.Backup_Status.setText("")
        self.Backup_Sofort.setText(QCoreApplication.translate("Ventilsteuerung", u"Backup sofort", None))
        self.Backup_Starten.setText(QCoreApplication.translate("Ventilsteuerung", u"Backup starten", None))
        self.Backup_Beenden.setText(QCoreApplication.translate("Ventilsteuerung", u"Backup beenden", None))
        self.Log_Exportieren.setText(QCoreApplication.translate("Ventilsteuerung", u"Log exportieren", None))
        self.pushButton.setText(QCoreApplication.translate("Ventilsteuerung", u"Reihenfolge Hochladen", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.Seite_Messung), QCoreApplication.translate("Ventilsteuerung", u"Messung", None))
        self.groupBox_4.setTitle(QCoreApplication.translate("Ventilsteuerung", u"Zusatzventil", None))
        self.label_12.setText(QCoreApplication.translate("Ventilsteuerung", u"Status:", None))
        self.Zusatz_Status.setText("")
        self.Zusatz_change.setText(QCoreApplication.translate("Ventilsteuerung", u"Starten", None))
        self.Zusatz_change_2.setText(QCoreApplication.translate("Ventilsteuerung", u"Stop", None))
        self.groupBox_6.setTitle(QCoreApplication.translate("Ventilsteuerung", u"Strommesung", None))
        self.Strommessen_Stop.setText(QCoreApplication.translate("Ventilsteuerung", u"Stop", None))
        self.Strommessen_Status.setText("")
        self.Strommessen_Start.setText(QCoreApplication.translate("Ventilsteuerung", u"Starten", None))
        self.label_13.setText(QCoreApplication.translate("Ventilsteuerung", u"Status:", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("Ventilsteuerung", u"Zusatz", None))
        self.groupBox.setTitle(QCoreApplication.translate("Ventilsteuerung", u"Tribox", None))
        self.label.setText(QCoreApplication.translate("Ventilsteuerung", u"ip:", None))
        self.Tribox_ip.setText(QCoreApplication.translate("Ventilsteuerung", u"192.168.178.15", None))
        self.label_2.setText(QCoreApplication.translate("Ventilsteuerung", u"port:", None))
        self.Tribox_port.setText(QCoreApplication.translate("Ventilsteuerung", u"502", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("Ventilsteuerung", u"UniPi", None))
        self.label_3.setText(QCoreApplication.translate("Ventilsteuerung", u"ip:", None))
        self.Unipi_ip.setText(QCoreApplication.translate("Ventilsteuerung", u"192.168.178.75", None))
        self.label_7.setText("")
        self.groupBox_3.setTitle(QCoreApplication.translate("Ventilsteuerung", u"Backup", None))
        self.Sciebolink.setText(QCoreApplication.translate("Ventilsteuerung", u"https://rwth-aachen.sciebo.de/s/V0kVRRL8JvdPdZi", None))
        self.label_4.setText(QCoreApplication.translate("Ventilsteuerung", u"ScieboLink:", None))
        self.label_5.setText(QCoreApplication.translate("Ventilsteuerung", u"Backup Periode (Tag):", None))
        self.Update_Periode.setText(QCoreApplication.translate("Ventilsteuerung", u"2", None))
        self.Backup_Zeit1111.setText(QCoreApplication.translate("Ventilsteuerung", u"Backup Zeit:", None))
        self.Backup_Zeit.setText(QCoreApplication.translate("Ventilsteuerung", u"00:00", None))
        self.groupBox_5.setTitle(QCoreApplication.translate("Ventilsteuerung", u"Strommessung", None))
        self.Strommessung_Periode.setText(QCoreApplication.translate("Ventilsteuerung", u"2", None))
        self.label_11.setText(QCoreApplication.translate("Ventilsteuerung", u"Intervall:", None))
        self.Einstellung_Hochladen.setText(QCoreApplication.translate("Ventilsteuerung", u"Einstellung hochladen", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("Ventilsteuerung", u"Einstellung", None))
    # retranslateUi

