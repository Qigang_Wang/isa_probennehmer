# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'Anmelden.ui'
##
## Created by: Qt User Interface Compiler version 6.4.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QHBoxLayout, QLabel, QMainWindow,
    QPushButton, QSizePolicy, QSpacerItem, QStatusBar,
    QVBoxLayout, QWidget)

class Ui_Anmelden(object):
    def setupUi(self, Anmelden):
        if not Anmelden.objectName():
            Anmelden.setObjectName(u"Anmelden")
        Anmelden.resize(365, 217)
        Anmelden.setMaximumSize(QSize(400, 300))
        self.centralwidget = QWidget(Anmelden)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalSpacer = QSpacerItem(20, 15, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_4)

        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setMinimumSize(QSize(100, 0))
        self.label.setMaximumSize(QSize(100, 16777215))

        self.horizontalLayout.addWidget(self.label)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.UniPi_Status = QPushButton(self.centralwidget)
        self.UniPi_Status.setObjectName(u"UniPi_Status")
        self.UniPi_Status.setEnabled(False)
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.UniPi_Status.sizePolicy().hasHeightForWidth())
        self.UniPi_Status.setSizePolicy(sizePolicy1)
        self.UniPi_Status.setMinimumSize(QSize(20, 20))
        self.UniPi_Status.setMaximumSize(QSize(20, 20))

        self.horizontalLayout.addWidget(self.UniPi_Status)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_3)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalSpacer_6 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_6)

        self.label_2 = QLabel(self.centralwidget)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setMinimumSize(QSize(100, 0))
        self.label_2.setMaximumSize(QSize(100, 16777215))

        self.horizontalLayout_2.addWidget(self.label_2)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_2)

        self.Tribox_Status = QPushButton(self.centralwidget)
        self.Tribox_Status.setObjectName(u"Tribox_Status")
        self.Tribox_Status.setEnabled(False)
        sizePolicy2 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.Tribox_Status.sizePolicy().hasHeightForWidth())
        self.Tribox_Status.setSizePolicy(sizePolicy2)
        self.Tribox_Status.setMinimumSize(QSize(20, 20))
        self.Tribox_Status.setMaximumSize(QSize(20, 20))

        self.horizontalLayout_2.addWidget(self.Tribox_Status)

        self.horizontalSpacer_5 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_5)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.verticalSpacer_2 = QSpacerItem(20, 15, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_2)

        self.Check_Verbindung = QPushButton(self.centralwidget)
        self.Check_Verbindung.setObjectName(u"Check_Verbindung")

        self.verticalLayout.addWidget(self.Check_Verbindung)

        self.Creatre_Verbindung = QPushButton(self.centralwidget)
        self.Creatre_Verbindung.setObjectName(u"Creatre_Verbindung")
        self.Creatre_Verbindung.setEnabled(False)

        self.verticalLayout.addWidget(self.Creatre_Verbindung)

        self.verticalSpacer_3 = QSpacerItem(20, 15, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_3)

        Anmelden.setCentralWidget(self.centralwidget)
        self.Infor = QStatusBar(Anmelden)
        self.Infor.setObjectName(u"Infor")
        Anmelden.setStatusBar(self.Infor)

        self.retranslateUi(Anmelden)
        self.Check_Verbindung.clicked.connect(Anmelden.CheckVerbindung)
        self.Creatre_Verbindung.clicked.connect(Anmelden.CreateVerbindung)

        QMetaObject.connectSlotsByName(Anmelden)
    # setupUi

    def retranslateUi(self, Anmelden):
        Anmelden.setWindowTitle(QCoreApplication.translate("Anmelden", u"HTK Ventilsteuerung Anmeldung", None))
        self.label.setText(QCoreApplication.translate("Anmelden", u"UniPi Status:", None))
        self.UniPi_Status.setText("")
        self.label_2.setText(QCoreApplication.translate("Anmelden", u"Tribox Status:", None))
        self.Tribox_Status.setText("")
        self.Check_Verbindung.setText(QCoreApplication.translate("Anmelden", u"Vernbindung \u00fcberpr\u00fcfen", None))
        self.Creatre_Verbindung.setText(QCoreApplication.translate("Anmelden", u"Verbindung herstellen", None))
    # retranslateUi

