import json
import sys
import time

from PySide6 import QtWidgets
from PySide6.QtCore import QObject, Signal, QThread
import ui_Ventilsteuerung
import ui_Anmelden
import socket
from Settings import Settings
from client import socket_client


# pyinstaller -F -w G:\isa_probennehmer\Ui\main.py

class ZustandOb(QObject):
    ZustandTrigger = Signal(int)  # 括号里是传出的参数的类型

    def __init__(self):
        super().__init__()
        # self.RunTrigger()

    def RunTrigger(self, Flag):  # args即为从主线程接收的参数
        while Flag:
            self.ZustandTrigger.emit(Flag)
            time.sleep(30)


class MyWidget(QtWidgets.QMainWindow):
    Setting = None
    Verbindung = None
    Observe = Signal(bool)
    LetztesZustand = ''

    def __init__(self):
        super().__init__()
        self.Initialisierung()

        self.UI_Anmelden = ui_Anmelden.Ui_Anmelden()
        self.UI_Ventilsteuerung = ui_Ventilsteuerung.Ui_Ventilsteuerung()
        self.UI_Anmelden.setupUi(self)

    def Initialisierung(self):
        self.Setting = Settings()

    def CheckVerbindung(self):
        self.UI_Anmelden.Infor.showMessage('Überprüfung der Verbindungen...', timeout=2000)
        if self.check_UniPi_Verbindung() & self.check_Tribox_Verbindung():
            self.UI_Anmelden.Creatre_Verbindung.setEnabled(True)
            self.UI_Anmelden.Infor.showMessage('Verbindungen exitiert.', timeout=2000)
        else:
            self.UI_Anmelden.Creatre_Verbindung.setEnabled(False)
            self.UI_Anmelden.Infor.showMessage('Verbindungen exitiert nicht.', timeout=2000)

    def CreateVerbindung(self):
        self.hide()
        self.UI_Ventilsteuerung.setupUi(self)
        self.show()
        self.UI_Ventilsteuerung.Infor.showMessage('Die letzte Messung wird eingeladen...', timeout=5000)
        self.Verbindung = socket_client()
        self.MessdatenAbholen()
        self.ZustandOberserve()
        self.SettingAbholen()

    def ZustandOberserve(self):
        self.Thread1 = QThread(self)
        self.Zustand_Observe = ZustandOb()
        self.Zustand_Observe.moveToThread(self.Thread1)

        self.Zustand_Observe.ZustandTrigger.connect(self.MessdatenAbholen)
        self.Observe.connect(self.Zustand_Observe.RunTrigger)
        self.Thread1.start()
        self.Observe.emit(True)

    def check_UniPi_Verbindung(self):
        sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sk.settimeout(1)
        try:
            sk.connect((self.Setting.Setting['UniPi']['ip'], 8080))
            self.setButtonColor(self.UI_Anmelden.UniPi_Status, 'green')
            Flag = True
        except BaseException:
            self.setButtonColor(self.UI_Anmelden.UniPi_Status, 'red')
            Flag = False
        sk.close()
        return Flag

    def check_Tribox_Verbindung(self):
        sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sk.settimeout(1)
        try:
            sk.connect((self.Setting.Setting['Tribox']['ip'], self.Setting.Setting['Tribox']['port']))
            self.setButtonColor(self.UI_Anmelden.Tribox_Status, 'green')
            Flag = True
        except BaseException:
            self.setButtonColor(self.UI_Anmelden.Tribox_Status, 'red')
            Flag = False
        sk.close()
        return Flag

    def setButtonColor(self, Button: QtWidgets.QPushButton, Color: str):
        Color_Red = "background-color: #FF6666;border-radius:5px"
        Color_Green = "background-color: #99CC66;border-radius:5px"
        Color_yellow = "background-color: #FFFF66;border-radius:5px"
        Color_grey = "background-color: #CCCCCC;border-radius:5px"
        if Color.lower() == 'red':
            Button.setStyleSheet(Color_Red)
        elif Color.lower() == 'green':
            Button.setStyleSheet(Color_Green)
        elif Color.lower() == 'yellow':
            Button.setStyleSheet(Color_yellow)
        elif Color.lower() == 'grey':
            Button.setStyleSheet(Color_grey)

    def MessdatenAbholen(self):
        try:
            self.Verbindung.TCP_Befehl_senden({'Befehl': 'getZustand'})
        except Exception as e:

            self.setButtonColor(self.UI_Ventilsteuerung.Messung_Status, 'yellow')
            self.setButtonColor(self.UI_Ventilsteuerung.Backup_Status, 'yellow')
            self.setButtonColor(self.UI_Ventilsteuerung.Zusatz_Status, 'yellow')
            QtWidgets.QMessageBox.critical(self, 'Keine Verbindung mehr!')
            return
            self.close()
        Zustand = json.loads(self.Verbindung.TCP_Nachricht_empfangen(self.Verbindung.tcp_socket))
        if self.LetztesZustand != Zustand:
            self.LetztesZustand = Zustand
            if Zustand['Messdaten'] == {}:
                self.UI_Ventilsteuerung.MessungText.setText('Es gibt noch keine Gültige Messung.')
                self.UI_Ventilsteuerung.AktuellVentil.setText('-')
            else:
                Zeigen = "{:15}: {} \r".format('Letzte Messung', Zustand['Messdaten']['Messzeit']['Wert'])
                Zeigen = Zeigen + "{:15}: {} \r".format('Ventil', Zustand['Messdaten']['Ventil']['Wert'])
                Konzentrationen = ['NO3_N', 'NO2_N', "CSBeq", "TSSeq", "SAK254", "ABS360", "ABS210", "Fit_Error",
                                   "ABS254",
                                   "NO3", "NO2", "COD_SACeq", "TOC_SACeq", "DOC_SACeq", "SQI", "UVT254"]
                for i in range(len(Konzentrationen)):
                    MessObjekt = Konzentrationen[i]
                    Zeigen = Zeigen + "{:15}: {:<10.2f} {} \r".format(
                        MessObjekt, Zustand['Messdaten'][MessObjekt]['Wert'],
                        Zustand['Messdaten'][MessObjekt]['Einheit'])
                    # Zeigen = Zeigen + f"{MessObjekt}: {Messdaten[MessObjekt]['Wert']} {Messdaten[MessObjekt]['Einheit']} \r"
                self.UI_Ventilsteuerung.MessungText.setStyleSheet('''QTextEdit {font: 10pt "Consolas";}''')
                self.UI_Ventilsteuerung.MessungText.setText(Zeigen)
                self.UI_Ventilsteuerung.AktuellVentil.setText(Zustand['Messdaten']['Ventil']['Wert'])
            self.setButtonColor(self.UI_Ventilsteuerung.Messung_Status, (Zustand['Messung'] and 'green' or 'red'))
            self.setButtonColor(self.UI_Ventilsteuerung.Backup_Status, (Zustand['Backup'] and 'green' or 'red'))
            self.setButtonColor(self.UI_Ventilsteuerung.Zusatz_Status, (Zustand['Zusatz'] and 'green' or 'red'))
            self.setButtonColor(self.UI_Ventilsteuerung.Strommessen_Status,
                                (Zustand['Strommessung'] and 'green' or 'red'))
            self.UI_Ventilsteuerung.Infor.showMessage('Messdanten werden abgeholt.', timeout=4000)
        else:
            pass

    def SettingAbholen(self):
        self.Verbindung.TCP_Befehl_senden({'Befehl': 'SettingAbholen'})
        Setting = json.loads(self.Verbindung.TCP_Nachricht_empfangen(conn=self.Verbindung.tcp_socket).replace(' ', ''))
        print(Setting)
        self.UI_Ventilsteuerung.Tribox_ip.setText(Setting['Tribox']['ip'])
        self.UI_Ventilsteuerung.Tribox_port.setText(str(Setting['Tribox']['port']))
        self.UI_Ventilsteuerung.Unipi_ip.setText(Setting['UniPi']['ip'])
        self.UI_Ventilsteuerung.Sciebolink.setText(Setting['Backup']['ScieboLink'])
        self.UI_Ventilsteuerung.Update_Periode.setText(str(Setting['Backup']['Backup_Periode']))
        self.UI_Ventilsteuerung.Backup_Zeit.setText(Setting['Backup']['Backup_Zeit'])
        self.UI_Ventilsteuerung.Strommessung_Periode.setText(str(Setting['Strommessung']['Intervall']))
        pass

    def MessungStarten(self):
        self.Verbindung.TCP_Befehl_senden({'Befehl': 'MessungStarten'})
        self.UI_Ventilsteuerung.Infor.showMessage('Das Messen wird gestartet.', timeout=5000)
        self.MessdatenAbholen()

    def Messungbeenden(self):
        self.Verbindung.TCP_Befehl_senden({'Befehl': 'MessungBeenden'})
        self.UI_Ventilsteuerung.Infor.showMessage('Das Messen wird beendet.', timeout=5000)
        self.MessdatenAbholen()

    def BackupStarten(self):
        self.Verbindung.TCP_Befehl_senden({'Befehl': 'BackupStarten'})
        self.UI_Ventilsteuerung.Infor.showMessage('Die Backupfunktion wird gestartet.', timeout=5000)
        self.MessdatenAbholen()

    def BackupBeenden(self):
        self.Verbindung.TCP_Befehl_senden({'Befehl': 'BackupBeenden'})
        self.UI_Ventilsteuerung.Infor.showMessage('Die Backupfunktion wird beendet.', timeout=5000)
        self.MessdatenAbholen()

    def BackupSofort(self):
        self.Verbindung.TCP_Befehl_senden({'Befehl': 'BackupSofort'})
        self.MessdatenAbholen()

    def MessdatenExportieren(self):
        self.Verbindung.TCP_Befehl_senden({'Befehl': 'MessdatenExportieren'})
        self.Verbindung.DateiEmpfangen()
        self.MessdatenAbholen()

    def EinstellungHochladen(self):
        self.Setting.Setting['Tribox']['ip'] = self.UI_Ventilsteuerung.Tribox_ip.text()
        self.Setting.Setting['Tribox']['port'] = int(self.UI_Ventilsteuerung.Tribox_port.text())
        self.Setting.Setting['UniPi']['ip'] = self.UI_Ventilsteuerung.Unipi_ip.text()
        self.Setting.Setting['Backup']['ScieboLink'] = self.UI_Ventilsteuerung.Sciebolink.text()
        self.Setting.Setting['Backup']['Backup_Periode'] = int(self.UI_Ventilsteuerung.Update_Periode.text())
        self.Setting.Setting['Backup']['Backup_Zeit'] = self.UI_Ventilsteuerung.Backup_Zeit.text()
        self.Setting.Setting['Strommessung']['Intervall'] = int(self.UI_Ventilsteuerung.Strommessung_Periode.text())
        self.Setting.save_settings()
        self.Verbindung.TCP_Befehl_senden({'Befehl': 'EinstellungChange', 'Wert': self.Setting.Setting})


    def ReihenFolgeHochladen(self):
        self.Verbindung.TCP_Befehl_senden({'Befehl': 'ReihenFolgeHochladen'})
        time.sleep(0.2)
        self.Verbindung.Datei_senden(self.Verbindung.tcp_socket, Filename='Reihenfolge.xlsx')
        # self.MessdatenAbholen()

    def LogHerunterladen(self):
        self.Verbindung.TCP_Befehl_senden({'Befehl': 'getLog'})
        self.Verbindung.DateiEmpfangen()
        self.MessdatenAbholen()

    def ZusatzVentilStarten(self):
        self.Verbindung.TCP_Befehl_senden({'Befehl': 'ZusatzVentilStarten'})
        self.UI_Ventilsteuerung.Infor.showMessage('Die Zusatzventilsteuerung wird gestartet.', timeout=5000)
        self.MessdatenAbholen()

    def ZusatzVentilStop(self):
        self.Verbindung.TCP_Befehl_senden({'Befehl': 'ZusatzVentilStop'})
        self.UI_Ventilsteuerung.Infor.showMessage('Die Zusatzventilsteuerung wird beendet.', timeout=5000)
        self.MessdatenAbholen()

    def StrommessenStarten(self):
        self.Verbindung.TCP_Befehl_senden({'Befehl': 'StromMessenStarten'})
        self.UI_Ventilsteuerung.Infor.showMessage('Die Strommessung wird gestartet.', timeout=5000)
        self.MessdatenAbholen()

    def StrommessenStop(self):
        self.Verbindung.TCP_Befehl_senden({'Befehl': 'StromMessenStop'})
        self.UI_Ventilsteuerung.Infor.showMessage('Die Strommessung wird beendet.', timeout=5000)
        self.MessdatenAbholen()

    def StromMessdatenExport(self):
        self.Verbindung.TCP_Befehl_senden({'Befehl': 'StromMessdatenExport'})
        self.Verbindung.DateiEmpfangen()
        self.MessdatenAbholen()



if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    app.setFont('Consolas')
    widget = MyWidget()
    widget.show()
    sys.exit(app.exec())
