import json


class Settings(object):
    SettingFileName = "Settings.json"
    Setting = {}

    def __init__(self):
        self.load_settings()

    def load_settings(self):
        with open(self.SettingFileName, "r") as f:
            Setting = json.load(f)
            f.close()
            for key in Setting:
                self.Setting[key] = Setting[key]

    def save_settings(self):
        File = open(self.SettingFileName, "w")
        File.write(json.dumps(self.Setting))
        File.close()

    def change_setting(self, key, value):
        self.Setting[key] = value
        self.save_settings()

if __name__ == "__main__":
    Setting = Settings()
    print(Setting.Setting)